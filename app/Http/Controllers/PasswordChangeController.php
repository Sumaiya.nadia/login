<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;

class PasswordChangeController extends Controller
{
    public function credential_rules(array $data)
{
  $messages = [
    'current-password.required' => 'Please enter current password',
    'password.required' => 'Please enter password',
  ];

  $validator = Validator::make($data, [
    'current-password' => ['required'],
    'password' => ['required', 'string', 'min:6', 'confirmed'],     
  ], $messages);

  
  //echo 'wrong';
  return $validator;
  
} 

public function postCredentials(Request $request)
{
  if(Auth::Check())
  {
    $request_data = $request->All();
    $validator = $this->credential_rules($request_data);
    if($validator->fails())
    {
      //response()->json(array('error' => $validator->getMessageBag()->toArray()), 400);
      Session::flash('message',$validator->getMessageBag()); 
      Session::flash('alert-class', 'alert-danger');
      return view('ChangePassword');
    }
    else
    {  
      $current_password = Auth::User()->password;           
      if(Hash::check($request_data['current-password'], $current_password))
      {           
        $user_id = Auth::User()->id;                       
        $obj_user = \App\User::find($user_id);
        $obj_user->password = Hash::make($request_data['password']);;
        $obj_user->save(); 
        Session::flash('message', 'Password changed successfully'); 
		    Session::flash('alert-class', 'alert-success');
		    return view('ChangePassword');
      }
      else
      {           
        Session::flash('message','Enter correct current password'); 
        Session::flash('alert-class', 'alert-danger');
        return view('ChangePassword');   
      }
    }        
  }
  else
  {
    return redirect()->to('/home');
  } 

}

}
